//See parser.h for source file information

#include <string>
#include <stdlib.h>
#include "scanner.h"
#include "token.h"
#include "node.h"

using namespace std;

//keep these locally global so less params need to be passed
static int totalLineCount = 1;
static char previousChar;
static token Token;

//few prototypes used since they're referenced in multiple places
struct node* exp(FILE* file);
struct node* block(FILE* file);
struct node* stat(FILE* file);

static string tokenNames[] = {"EOFTk", "IntTk", "IDTk", "AssignTk", "ComparTk", "IncrTk", "DecrTk", "ColTk", "SemColTk", "PeriodTk", "ComTk", "GTEQTk", 
                            "LTEQTk", "FSlashTk", "StarTK", "PercentTk", "LCBraceTk", "RCBraceTk", "LSBraceTk", "RSBraceTk", "LParenTk", "RParenTk", "KeywordTk"};

//function to initialize nodes
struct node* newNode(string label)
{
    struct node* temp = new node;
    temp->label = label;
    temp->c1 = NULL;
    temp->c2 = NULL;
    temp->c3 = NULL;
    temp->c4 = NULL;
    return temp;
}

//from RO function to program function below, these are the code equivalents of BNF
struct node* RO(FILE* file)
{
    node* root = newNode("RO");
    if (Token.tokenValue == LSEQTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        return root;
    }
    else if (Token.tokenValue == GTEQTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        return root;
    }
    else if (Token.tokenValue == AssignTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        return root;
    }
    else if (Token.tokenValue == PercentTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        return root;
    }
    else
    {
        printf("PARSING ERROR: LSEQTk, GTEQTk, AssignTk or PercentTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* assign(FILE* file)
{
    node* root = newNode("assign");
    if (Token.tokenValue == IdTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        if (Token.tokenValue == ComparTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            root->c1 = exp(file);
            return root;
        }
        else
        {
            printf("PARSING ERROR: ComparTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);  
        }
    }
    else
    {
        printf("PARSING ERROR: IDTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);  
    }
}

struct node* loop(FILE* file)
{
    node* root = newNode("loop");
    if (Token.instance == "loop")
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        if (Token.tokenValue == LParenTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            root->c1 = exp(file);
            root->c2 = RO(file);
            root->c3 = exp(file);
            if (Token.tokenValue == RParenTk)
            {
                root->tokens.push_back(Token);
                Token = FSMDriver(file, totalLineCount, previousChar, false);
                totalLineCount = Token.lineNumber;
                root->c4 = stat(file);
                return root;
            }
            else
            {
                printf("PARSING ERROR: RParenTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                fclose(file);
                exit(1);  
            }
        }
        else
        {
            printf("PARSING ERROR: LParenTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
    {
        printf("PARSING ERROR: keyword loop expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* IF(FILE* file)
{
    node* root = newNode("IF");
    if (Token.instance == "fork")
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        if (Token.tokenValue == LParenTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            root->c1 = exp(file);
            root->c2 = RO(file);
            root->c3 = exp(file);
            if (Token.tokenValue == RParenTk)
            {
                root->tokens.push_back(Token);
                Token = FSMDriver(file, totalLineCount, previousChar, false);
                totalLineCount = Token.lineNumber;
                if (Token.instance == "then")
                {
                    root->tokens.push_back(Token);
                    Token = FSMDriver(file, totalLineCount, previousChar, false);
                    totalLineCount = Token.lineNumber;
                    root->c4 = stat(file);
                    return root;
                }
                else
                {
                    printf("PARSING ERROR: keyword then expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                    fclose(file);
                    exit(1);
                }
            }
            else
            {
                printf("PARSING ERROR: RParenTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                fclose(file);
                exit(1);  
            }
        }
        else
        {
            printf("PARSING ERROR: LParenTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
    {
        printf("PARSING ERROR: keyword fork expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* out(FILE* file)
{
    node* root = newNode("out");
    if (Token.instance == "print")
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        if (Token.tokenValue == LParenTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            root->c1 = exp(file);
            if (Token.tokenValue == RParenTk)
            {
                root->tokens.push_back(Token);
                Token = FSMDriver(file, totalLineCount, previousChar, false);
                totalLineCount = Token.lineNumber;
                return root;
            }
            else
            {
                printf("PARSING ERROR: RParenTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                fclose(file);
                exit(1);  
            }
        }
        else
        {
            printf("PARSING ERROR: LParenTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
    {
        printf("PARSING ERROR: keyword print expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* in(FILE* file)
{
    node* root = newNode("in");
    if (Token.instance == "scan")
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        if (Token.tokenValue == IdTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: IDTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
    {
        printf("PARSING ERROR: keyword scan expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* stat(FILE* file)
{
    node* root = newNode("stat");
    if (Token.instance == "scan")
    {
        root->c1 = in(file);
        if (Token.tokenValue == SemColTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: SemColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else if (Token.instance == "print")
    {
        root->c1 = out(file);
        if (Token.tokenValue == SemColTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: SemColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else if (Token.instance == "fork")
    {
        root->c1 = IF(file);
        if (Token.tokenValue == SemColTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: SemColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else if (Token.instance == "begin")
    {
        root->c1 = block(file);
        return root;
    }
    else if (Token.instance == "loop")
    {
        root->c1 = loop(file);
        if (Token.tokenValue == SemColTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: SemColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else if (Token.tokenValue == IdTk)
    {
        root->c1 = assign(file);
        if (Token.tokenValue == SemColTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: SemColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
    {
        printf("PARSING ERROR: IDTk or keyword scan, print, fork, loop or begin expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* mStat(FILE* file)
{
    node* root = newNode("mStat");
    if (Token.instance == "scan" || Token.instance == "print" || Token.instance == "fork" || Token.instance == "begin" || Token.instance == "loop" || Token.tokenValue == IdTk)
    {
        root->c1 = stat(file);
        root->c2 = mStat(file);
        return root;
    }
    else
        return root;
}

struct node* stats(FILE* file)
{
    node* root = newNode("stats");
    root->c1 = stat(file);
    root->c2 = mStat(file);
    return root;
}

struct node* R(FILE* file)
{
    node* root = newNode("R");
    if (Token.tokenValue == LSBraceTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c1 = exp(file);
        if (Token.tokenValue == RSBraceTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: RSBraceTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else if (Token.tokenValue == IdTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        return root;
    }
    else if (Token.tokenValue == IntTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        return root;
    }
    else
    {
        printf("PARSING ERROR: LSBraceTk, IntTk or IDTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* M(FILE* file)
{
    node* root = newNode("M");
    if (Token.tokenValue == DecrTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c1 = M(file);
        return root;
    }
    else if (Token.tokenValue == LSBraceTk || Token.tokenValue == IdTk || Token.tokenValue == IntTk)
    {
        root->c1 = R(file);
        return root;
    }
    else
    {
        printf("PARSING ERROR: LSBraceTk, DecrTk, IntTk or IDTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* N(FILE* file)
{
    node* root = newNode("N");
    root->c1 = M(file);
    if (Token.tokenValue == FSlashTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c2 = N(file);
        return root;
    }
    else if (Token.tokenValue == StarTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c2 = N(file);
        return root;
    }
    else
        return root;
}

struct node* A(FILE* file)
{
    node* root = newNode("A");
    root->c1 = N(file); 
    if (Token.tokenValue == DecrTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c2 = A(file);
        return root;
    }
    else
        return root;
}

struct node* exp(FILE* file)
{
    node* root = newNode("exp");
    root->c1 = A(file);
    if (Token.tokenValue == IncrTk)
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c2 = exp(file);
        return root;
    }
    else
        return root;
}

struct node* vars(FILE* file)
{
    node* root = newNode("vars");
    if (Token.instance == "var")
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        if (Token.tokenValue == IdTk)
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            if (Token.tokenValue == ColTk)
            {
                root->tokens.push_back(Token);
                Token = FSMDriver(file, totalLineCount, previousChar, false);
                totalLineCount = Token.lineNumber;
                if (Token.tokenValue == IntTk)
                {
                    root->tokens.push_back(Token);
                    Token = FSMDriver(file, totalLineCount, previousChar, false);
                    totalLineCount = Token.lineNumber;
                    if (Token.tokenValue == SemColTk)
                    {
                        root->tokens.push_back(Token);
                        Token = FSMDriver(file, totalLineCount, previousChar, false);
                        totalLineCount = Token.lineNumber;

                        root->c1 = vars(file);
                        return root;
                    }
                    else
                    {
                        printf("PARSING ERROR: SemColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                        fclose(file);
                        exit(1);
                    }
                }
                else
                {
                    printf("PARSING ERROR: IntTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                    fclose(file);
                    exit(1);
                }
            }
            else
            {
                printf("PARSING ERROR: ColTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
                fclose(file);
                exit(1);
            }
        }
        else
        {
            printf("PARSING ERROR: IDTk expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
        return root;
}

struct node* block(FILE* file)
{
    node* root = newNode("block");
    if (Token.instance == "begin")
    {
        root->tokens.push_back(Token);
        Token = FSMDriver(file, totalLineCount, previousChar, false);
        totalLineCount = Token.lineNumber;
        root->c1 = vars(file);
        root->c2 = stats(file);
        if (Token.instance == "end")
        {
            root->tokens.push_back(Token);
            Token = FSMDriver(file, totalLineCount, previousChar, false);
            totalLineCount = Token.lineNumber;
            return root;
        }
        else
        {
            printf("PARSING ERROR: end keyword expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
            fclose(file);
            exit(1);
        }
    }
    else
    {
        printf("PARSING ERROR: begin keyword expected at line %i, %s received. Program terminating\n", totalLineCount, tokenNames[Token.tokenValue].c_str());
        fclose(file);
        exit(1);
    }
}

struct node* program(FILE* file)
{
    node* root = newNode("program");
    root->c1 = vars(file);
    root->c2 = block(file);
    return root;
}

//the auxillary function for the parser. Calls preliminary BNF function.
struct node* parser(FILE* file)
{
    bool firstChar = true;
    Token = FSMDriver(file, totalLineCount, previousChar, firstChar);
    totalLineCount = Token.lineNumber;
    firstChar = false;

    node* root = program(file);
    if (Token.tokenValue == EOFTk)
    {
        fclose(file);
        return root;
    }
    else
    {
        printf("PARSING ERROR: EOFTk expected at end of file but %s received, parse failed\n", Token.instance.c_str());
        fclose(file);
        exit(1);
    }
}

//function used to delete the tree and free up memory
void destroySubTree(node* root)
{
    if (root)
    {
        if(root->c1)
            destroySubTree(root->c1);
        if(root->c2)
            destroySubTree(root->c2);
        if(root->c3)
            destroySubTree(root->c3);
        if(root->c4)
            destroySubTree(root->c4);
        delete root;
    }
}