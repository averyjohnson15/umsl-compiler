//Token definitions to be used in lexical analysis and further compilation steps

#ifndef TOKEN_H
#define TOKEN_H

#include <stdio.h>
#include <string>

enum tokenID {EOFTk, IntTk, IdTk, AssignTk, ComparTk, IncrTk, DecrTk, ColTk, SemColTk, PeriodTk, ComTk, GTEQTk, LSEQTk, 
                    FSlashTk, StarTk, PercentTk, LCBraceTk, RCBraceTk, LSBraceTk, RSBraceTk, LParenTk, RParenTk, KeyTk, ERROR = -1};

typedef struct token
{
    tokenID tokenValue;
    std::string instance;
    int lineNumber;
}token;

#endif