//See valAndGen.h for source file information

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string>
#include <iostream>
#include <sstream>
#include <stack>
#include "node.h"
#include "token.h"

using namespace std;

//output file to be used for semantics validation
static FILE* outFile;

//struct to keep track of varTk and its depth, used in stack for satSems
typedef struct stackItem
{
    token stackToken;
    int tokenDepth;
}stackItem;
static stack<stackItem> Stack;
static int currentDepth = 0;
static int varCtr = 0;
static int lblCtr = 0;
typedef enum {VAR, LABEL} nameType;

//func to find the current offset of an IDTk
int findOffset(string varInstance)
{
    stack<stackItem> tempStack;
    int offset = 0;
    int tempCount = 0;
    bool found = false;
    while (!Stack.empty())
    {
        if (Stack.top().stackToken.instance == varInstance && !found)
        {
            offset = tempCount;
            found = true;
        }  
        tempStack.push(Stack.top());
        Stack.pop();
        tempCount++;
    }
    while (!tempStack.empty())
    {
        Stack.push(tempStack.top());
        tempStack.pop();
    }
    return offset;
}

//func to create new labels and vars
string newName(nameType what)
{ 
    string temp;
    if (what == VAR) // creating new temp var
    {
        string snippit;
        stringstream tempStream;
        tempStream << varCtr;
        tempStream >> snippit;
        temp = "T";
        temp.append(snippit);
        varCtr++;
    }
    else // creating new Label
    {
        string snippit;
        stringstream tempStream;
        tempStream << lblCtr;
        tempStream >> snippit;
        temp = "L";
        temp.append(snippit);
        lblCtr++;
    }
    return(temp);
}

//helper function to check for incorrect variable use
void statSemVal(struct node* root)
{
    bool IDFound = false;
    int tokenNumber = 0;
    for (int i = 0; i < root->tokens.size() ; i++)
    {
        if (root->tokens[i].tokenValue == IdTk)
        {
            IDFound = true;
            tokenNumber = i;
        }
    }
    if (IDFound)
    {
        bool isUsed = false;
        stack<stackItem> tempStack;
        while (!Stack.empty())
        {
            tempStack.push(Stack.top());
            Stack.pop();
            if (tempStack.top().stackToken.instance == root->tokens[tokenNumber].instance)
                isUsed = true;
        }
        if (!isUsed)
        {
            printf("STAT SMTCS ERROR: Attempted to use IDTk %s at line %i, but this IDTk is not declared before use.\n", root->tokens[tokenNumber].instance.c_str(), root->tokens[tokenNumber].lineNumber);
            exit(1);
        }
        while (!tempStack.empty())
        {
            Stack.push(tempStack.top());
            tempStack.pop();
        }
    }
}

//function to write the preorder traversal of the tree to the terminal
void codeGen(struct node* root, int& localNum)
{
    if (root != NULL)
    {
        //if label on node is vars, check if var has been declared in scope, add if not
        if (root->label == "vars")
        {
            if (root->c1 == NULL)
                return;
            stack<stackItem> tempStack;
            while (!Stack.empty())
            {
                tempStack.push(Stack.top());
                Stack.pop();
                if (tempStack.top().stackToken.instance == root->tokens[1].instance && tempStack.top().tokenDepth == currentDepth)
                {
                    printf("STAT SMTCS ERROR: variable %s was declared at line %i. An attempt was made to declare it again in the same scope at line %i.\n", root->tokens[1].instance.c_str(), tempStack.top().stackToken.lineNumber, root->tokens[1].lineNumber);
                    exit(1);
                }
            }
            while (!tempStack.empty())
            {
                Stack.push(tempStack.top());
                tempStack.pop();
            }
            localNum++;
            stackItem temp = {root->tokens[1], currentDepth};
            Stack.push(temp);
            fprintf(outFile, "PUSH\nLOAD %s\nSTACKW 0\n", root->tokens[3].instance.c_str());
            codeGen(root->c1, localNum);
            return;
        }
        //if label for node is block, add to static depth, remove static depth upon reaching the end of node
        else if (root->label == "block")
        {
            currentDepth++;
            int localCount = 0;
            codeGen(root->c1, localCount);
            codeGen(root->c2, localCount);
            for (int i = 0 ; i < localCount ; i++)
            {
                Stack.pop();
                fprintf(outFile, "POP\n");
            }
            currentDepth--;
            return;
        }
        //if label on node is equal to anything other than block or var, check relative stat sems if needed and generate code
        else if (root->label == "program")
        {
            codeGen(root->c1, localNum);
            codeGen(root->c2, localNum);
            return;
        }
        else if (root->label == "exp")
        {
            if (root->c2 != NULL)
            {
                string temp = newName(VAR);
                codeGen(root->c2, localNum);
                fprintf(outFile, "STORE %s\n", temp.c_str());
                codeGen(root->c1, localNum);
                fprintf(outFile, "ADD %s\n", temp.c_str());
                return;
            }
            else
            {
                codeGen(root->c1, localNum);
                return;
            }
        }
        else if (root->label == "A")
        {
            if (root->c2 != NULL)
            {
                string temp = newName(VAR);
                codeGen(root->c2, localNum);
                fprintf(outFile, "STORE %s\n", temp.c_str());
                codeGen(root->c1, localNum);
                fprintf(outFile, "SUB %s\n", temp.c_str());
                return;
            }
            else
            {
                codeGen(root->c1, localNum);
                return;
            }
        }
        else if (root->label == "N")
        {
            if (root->c2 != NULL)
            {
                string temp = newName(VAR);
                codeGen(root->c2, localNum);
                fprintf(outFile, "STORE %s\n", temp.c_str());
                codeGen(root->c1, localNum);
                if (root->tokens[0].tokenValue == StarTk)
                    fprintf(outFile, "MULT %s\n", temp.c_str());
                else
                    fprintf(outFile, "DIV %s\n", temp.c_str());
                return;
            }
            else
            {
                codeGen(root->c1, localNum);
                return;
            }
        }
        else if (root->label == "M")
        {
            if (root->tokens.size() > 0)
            {
                string temp = newName(VAR);
                string negLbl = newName(LABEL);
                string posLbl = newName(LABEL);
                codeGen(root->c1, localNum);
                fprintf(outFile, "STORE %s\n", temp.c_str());
                fprintf(outFile, "BRNEG %s\n", negLbl.c_str());
                fprintf(outFile, "SUB %s\n", temp.c_str());
                fprintf(outFile, "SUB %s\n", temp.c_str());
                fprintf(outFile, "BR %s\n", posLbl.c_str());
                fprintf(outFile, "%s: SUB %s\n", negLbl.c_str(), temp.c_str());
                fprintf(outFile, "SUB %s\n", temp.c_str());
                fprintf(outFile, "%s: NOOP\n", posLbl.c_str());
                return;
            }
            else
            {
                codeGen(root->c1, localNum);
                return;
            }
            codeGen(root->c1, localNum);
            codeGen(root->c2, localNum);
            codeGen(root->c3, localNum);
            codeGen(root->c4, localNum);
        }
        else if (root->label == "R")
        {
            statSemVal(root);

            if (root->tokens[0].tokenValue == IdTk)
            {
                fprintf(outFile, "STACKR %i\n", findOffset(root->tokens[0].instance));
                return;
            }
            else if (root->tokens[0].tokenValue == IntTk)
            {
                fprintf(outFile, "LOAD %s\n", root->tokens[0].instance.c_str());
                return;
            }
            else
            {
                codeGen(root->c1, localNum);
                return;
            }
        }
        else if (root->label == "stats")
        {
            codeGen(root->c1, localNum);
            codeGen(root->c2, localNum);
            return;
        }
        else if (root->label == "mStat")
        {
            if (root->c1 == NULL)
                return;
            else
            {
                codeGen(root->c1, localNum);
                codeGen(root->c2, localNum);
                return;
            }

        }
        else if (root->label == "stat")
        {
            codeGen(root->c1, localNum);
            return;
        }
        else if (root->label == "in")
        {
            statSemVal(root);
            string temp = newName(VAR);
            fprintf(outFile, "READ %s\n", temp.c_str()); //read temp
            fprintf(outFile, "LOAD %s\n", temp.c_str()); //load temp to acc
            fprintf(outFile, "STACKW %i\n", findOffset(root->tokens[1].instance)); //store temp in stack
            return;
        }
        else if (root->label == "out")
        {
            codeGen(root->c1, localNum);
            string temp = newName(VAR);
            fprintf(outFile, "STORE %s\n", temp.c_str());
            fprintf(outFile, "WRITE %s\n", temp.c_str());
            return;
        }
        else if (root->label == "IF")
        {
            codeGen(root->c3, localNum);
            string temp = newName(VAR);
            fprintf(outFile, "STORE %s\n", temp.c_str());
            codeGen(root->c1, localNum);
            fprintf(outFile, "SUB %s\n", temp.c_str());
            string tempLbl = newName(LABEL);

            if (root->c2->tokens[0].tokenValue == AssignTk)
            {
                fprintf(outFile,"BRNEG %s\n",tempLbl.c_str());
                fprintf(outFile,"BRPOS %s\n",tempLbl.c_str());
            }
            else if (root->c2->tokens[0].tokenValue == PercentTk)
            {
                fprintf(outFile,"BRZERO %s\n",tempLbl.c_str());
            }
            else if (root->c2->tokens[0].tokenValue == LSEQTk)
            {
                fprintf(outFile,"BRPOS %s\n",tempLbl.c_str());
            }
            else if (root->c2->tokens[0].tokenValue == GTEQTk)
            {
                fprintf(outFile,"BRNEG %s\n",tempLbl.c_str());
            }

            codeGen(root->c4, localNum);
            fprintf(outFile, "%s: NOOP\n", tempLbl.c_str());
            return;
        }
        else if (root->label == "loop")
        {
            string tempVar = newName(VAR);
            string tempOut = newName(LABEL);
            string tempIter = newName(LABEL);
            fprintf(outFile,"%s: NOOP\n",tempIter.c_str());
            codeGen(root->c3, localNum);
            string temp = newName(VAR);
            fprintf(outFile, "STORE %s\n", temp.c_str());
            codeGen(root->c1, localNum);
            fprintf(outFile, "SUB %s\n", temp.c_str());

            if (root->c2->tokens[0].tokenValue == AssignTk)
            {
                fprintf(outFile,"BRNEG %s\n",tempOut.c_str());
                fprintf(outFile,"BRPOS %s\n",tempOut.c_str());
            }
            else if (root->c2->tokens[0].tokenValue == PercentTk)
            {
                fprintf(outFile,"BRZERO %s\n",tempOut.c_str());
            }
            else if (root->c2->tokens[0].tokenValue == LSEQTk)
            {
                fprintf(outFile,"BRPOS %s\n",tempOut.c_str());
            }
            else if (root->c2->tokens[0].tokenValue == GTEQTk)
            {
                fprintf(outFile,"BRNEG %s\n",tempOut.c_str());
            }

            codeGen(root->c4, localNum);
            fprintf(outFile,"BR %s\n",tempIter.c_str());
            fprintf(outFile,"%s: NOOP\n",tempOut.c_str());
        }
        else if (root->label == "assign")
        {
            statSemVal(root);
            codeGen(root->c1, localNum);
            fprintf(outFile, "STACKW %i\n", findOffset(root->tokens[0].instance));
        }
        else if (root->label == "RO")
        {
            return;
        }
    }
}

//function used to instantiate temp vars used after STOP is printed
void instVars(FILE* outFile)
{
    for(int i = 0 ; i < varCtr ; i++)
        fprintf(outFile, "T%i 0\n", i);
}

//used for entry point in codeGen since it required a var since it's using pass by reference in params
int start;
void validationAndGeneration(struct node* root, string path)
{
    outFile = fopen(path.c_str(), "w");
    codeGen(root, start);
    fprintf(outFile, "STOP\n");
    instVars(outFile);
    fclose(outFile);
}