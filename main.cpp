/*******************************************************************************************************
Created by: Avery Johnson
Course/Project: CS4280/P4
Date Due: December 14, 2022

Please see README for further information.

Sources: - No outside sources were used for P4 (Compiler for CS4280)
*******************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <io.h>
#include "parser.h"
#include "valAndGen.h"
#include "node.h"

using namespace std;

void testFile(FILE* file);

//entry point
int main(int argc, char* argv[])
{
    FILE* file = fdopen(0, "r");
    string singledName = "";

    //Shell takes over if 1 arg(just ./comp) is used or if file is piped to stdin, so if any arg count above 2, print error and instructions.
    if (argc == 2)
    {
        fclose(file);
        string tempName = singledName = argv[1];
        tempName += ".cs4280";
        file = fopen(tempName.c_str(), "r");
    }
    //check if stdin is from a terminal device. If not then test the file and rewind.
    if (!isatty(0))
    {
        testFile(file);
        rewind(file);
    }
    else if (argc > 2)
    {
        printf("ERROR: Invalid arguments input. 'comp textfile' for standard use, 'comp < textfile.4280' to input file directly,\nor 'comp' to record input from keyboard (enter then ctrl+D to terminate keyboard input).\n");
        exit(1);
    }
    if (singledName == "")
        singledName = "a";
    string outFileName = singledName;
    outFileName += ".asm";

    node* root = parser(file); //lexical analysis and parsing
    validationAndGeneration(root, outFileName); //validate semantics and generate code
    destroySubTree(root); //clear out tree
    printf("%s\n", outFileName.c_str());
    return 0;
}

//test if file exists or if it is empty (don't run if no arguments aside from './comp' are used).
void testFile(FILE* file)
{
    if (!file)
    {
        printf("ERROR: The file you attempted to input doesn't exist (extension is added for you if file isn't piped in).\nPlease try again or enter 'comp' and manually enter the desired input (enter then ctrl+D to terminate input).\n");
        exit(1);
    }
    int c = fgetc(file);
    while (isspace(c))
    {
        c = fgetc(file);
    }
    if (c == EOF)
    {
        printf("ERROR: The file you attempted to input is empty (extension is added for you if file isn't piped in).\nPlease try again or enter 'comp' and manually enter the desired input (enter then ctrl+D to terminate input).\n");
        exit(1);
    }
}