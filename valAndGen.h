//Source code for semantic validation and Assembly generation

#ifndef VALGEN_H
#define VALGEN_H

#include <string>

void validationAndGeneration(struct node* root, std::string path);

#endif