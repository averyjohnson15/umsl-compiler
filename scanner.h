//Source code for lexical analysis of input file

#ifndef SCANNER_H
#define SCANNER_H

#include <cstdio>
#include "token.h"

token FSMDriver(FILE* file, int lineCount, char& pastChar, bool firstChar);

#endif