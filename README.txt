**NOT ORIGINAL REPOSITORY, FILES COPIED TO REMOVE SENSITIVE INFORMATION**

Storage=Local
Working Tests: (All Tests) - Code Generation test 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 & local Storage Allocation test
Non-Working Tests: (None) - There are currently no tests that aren't working

Created by: Avery Johnson
Course/Project: CS4280/P4
Date Due: December 14, 2022

The project currently has no known bugs and works as expected (at least throughout my testing).

Invocations:
comp arg - Use file without extension as a single argument. Extension will be added by program.
comp - With no arguments and no piping, enter only "comp" to use keyboard for STDIN. Use ctrl+D to end input.
comp < arg.cs4280 - Pipe file for direct use through STDIN. Ensure extension is used.

Sources: - No external sources were used for P4.