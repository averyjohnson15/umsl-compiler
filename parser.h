//Source code to create parse tree for further steps in compilation

#ifndef PARSER_H
#define PARSER_H

#include <cstdio>

struct node* parser(FILE* file);
void destroySubTree(node* root);

#endif