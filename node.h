//struct header to define value of nodes in parse tree

#ifndef NODE_H
#define NODE_H

#include <stdio.h>
#include <string>
#include <vector>
#include "token.h"

typedef struct node
{
    std::string label;
    std::vector<token> tokens;

    struct node* c1;
    struct node* c2;
    struct node* c3;
    struct node* c4;
}node;

#endif