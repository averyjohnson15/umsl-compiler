//See scanner.h for source file information

#include <string>
#include <cstdio>
#include <stdlib.h>
#include "token.h"

using namespace std;

//find column value for character using ASCII values
int columnLocator(char c)
{
    if (c >= 65 && c <= 90)//0 for uppercase L
        return 0;
    else if (c >= 97 && c <= 122)//1 for lowercase L
        return 1;
    else if (c >= 48 && c <= 57)//2 for digit
        return 2;
    else if (c == 61)//3 for =
        return 3;
    else if (c == 62)//4 for >
        return 4;
    else if (c == 60)//5 for <
        return 5;
    else if (c == 58)//6 for :
        return 6;
    else if (c == 43)//7 for +
        return 7;
    else if (c == 45)//8 for -
        return 8;
    else if (c == 42)//9 for *
        return 9;
    else if (c == 47)//10 for /
        return 10;
    else if (c == 37)//11 for %
        return 11;
    else if (c == 46)//12 for .
        return 12;
    else if (c == 40)//13 for (
        return 13;
    else if (c == 41)//14 for )
        return 14;
    else if (c == 44)//15 for ,
        return 15;
    else if (c == 123)//16 for {
        return 16;
    else if (c == 125)//17 for }
        return 17;
    else if (c == 91)//18 for [
        return 18;
    else if (c == 93)//19 for ]
        return 19;
    else if (c == 59)//20 for ;
        return 20;
    else if (c == EOF)//21 for EOF
        return 21;
    else
        return 22;//otherwise return 22 for unidentified char
}

//function to filter out spaces, lines, EOF, unregistered chars, etc. Returns linecount
int filter(FILE* file, int lineTally, char& previousChar, bool firstChar)
{
    //local linecount
    int LC = 0;
    char c;
    if (firstChar)
        c = fgetc(file);
    else
        c = previousChar;
    while(c < 37 || c > 37 && c < 40 || c == 63 || c == 64 || c == 92 || c > 93 && c < 97 || c == 124 || c > 125)
    {
        if (c == '\n')//get next char and increment linecount if next char is newline
        {
            LC++;
            c = fgetc(file);
        }
        while (c == ' ')
        {
            c = fgetc(file);
        }
        //check for comments after spacing, throw error if comment is open when EOFTk is processed
        if (c == 35)
        {
            c = fgetc(file);
            while (c != 35 && c != EOF)
            {
                if (c == '\n')
                {
                    LC++;
                    c = fgetc(file);
                }
                else
                    c = fgetc(file);
            }
            if (c == EOF)
            {
                printf("EOFTk\nLEXICAL ERROR: EOFTk processed before comment was closed\n");
                fclose(file);
                exit(1);
            }
            if (c == 35)
                c = fgetc(file);
        }
        if (c == EOF)
        {
            previousChar = c;
            return lineTally + LC;
        }
        
        //check if character is part of alphabet
        if (c != '\n' && c != EOF && c < 32 || c > 32 && c < 37 || c > 37 && c < 40 || c == 63 || c == 64 || c == 92 || c > 93 && c < 97 || c == 124 || c > 125)
        {
            printf("LEXICAL ERROR: Unidentified Character entered at line %i: %c\n", lineTally + LC, c);
            fclose(file);
            exit(1);
        }
    }
    if (c == EOF)
    {
        previousChar = c;
        return lineTally + LC;
    }

    previousChar = c;
    return lineTally + LC;
}

token FSMDriver(FILE* file, int lineCount, char& previousChar, bool firstChar)
{
    string keywords[14] = {"begin", "end", "loop", "void", "var", "exit", "scan", "print", "main", "fork", "then", "let", "data", "func"};

    //lookup table for FSM
    int FSMTable[26][23] = {
                            {-2, 1, 2, 3, 5, 6, 9, 10, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 1001, -1},
                            {1, 1, 1, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003, 1003},
                            {1002, 1002, 2, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002, 1002},
                            {1004, 1004, 1004, 4, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004, 1004},
                            {1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005, 1005},
                            {-3, -3, -3, 7, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3},
                            {-4, -4, -4, 8, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
                            {1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012, 1012},
                            {1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013, 1013},
                            {1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008, 1008},
                            {-5, -5, -5, -5, -5, -5, -5, 11, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5},
                            {1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006, 1006},
                            {-6, -6, -6, -6, -6, -6, -6, -6, 13, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6, -6},
                            {1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007, 1007},
                            {1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015, 1015},
                            {1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014, 1014},
                            {1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016, 1016},
                            {1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010},
                            {1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021, 1021},
                            {1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022, 1022},
                            {1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011, 1011},
                            {1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017, 1017},
                            {1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018, 1018},
                            {1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019, 1019},
                            {1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020, 1020},
                            {1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009, 1009},
                            };

    token tempToken;
    //run filter to get linecount and get rid of spaces, comments, etc
    tempToken.lineNumber = filter(file, lineCount, previousChar, firstChar);
    string tokenInstance = "";

    //get initial column value for lookup table and find initial value at relative position
    int columnCount = columnLocator(previousChar);
    int tableValue = FSMTable[0][columnCount];
    //loop through lookup table until error or final state is located
    while (tableValue >= 0 && tableValue < 1000)
    {
        tokenInstance.push_back(previousChar);
        previousChar = fgetc(file);
        columnCount = columnLocator(previousChar);
        int tempValue = FSMTable[tableValue][columnCount];
        tableValue = tempValue;
    }
    //set token instance to the string value that's declared before while loop, then print a relative error if needed
    if (tableValue == -2)
    {
        tempToken.tokenValue = ERROR;
        tokenInstance.push_back(previousChar);
        previousChar = fgetc(file);
        printf("LEXICAL ERROR: %s at line %i - No tokens begin with a capital letter\n", tokenInstance.c_str(), tempToken.lineNumber);
        fclose(file);
        exit(1);
    }
    tempToken.instance = tokenInstance;
    if (tableValue == -3)
    {
        tempToken.tokenValue = ERROR;
        printf("LEXICAL ERROR: At line %i - '>' not valid token, '=' expected after\n", tempToken.lineNumber);
        fclose(file);
        exit(1);
    }
    if (tableValue == -4)
    {
        tempToken.tokenValue = ERROR;
        printf("LEXICAL ERROR: At line %i - '<' not valid token, '=' expected after\n", tempToken.lineNumber);
        fclose(file);
        exit(1);
    }
    if (tableValue == -5)
    {
        tempToken.tokenValue = ERROR;
        printf("LEXICAL ERROR: At line %i - '+' not valid token, '+' expected after\n", tempToken.lineNumber);
        fclose(file);
        exit(1);
    }
    if (tableValue == -6)
    {
        tempToken.tokenValue = ERROR;
        printf("LEXICAL ERROR: At line %i - '-' not valid token, '-' expected after\n", tempToken.lineNumber);
        fclose(file);
        exit(1);
    }
    //if the final state is an identifier, check if it is on the list of keywords
    if (tableValue == 1003)
    {
        bool keyFlag = false;
        for (int i = 0 ; i < 14 ; i++)
        {
            if (tempToken.instance == keywords[i])
            {
                keyFlag = true;
                tempToken.tokenValue = KeyTk;
                break;
            }
        }
        if (!keyFlag)
            tempToken.tokenValue = IdTk;
    }
    //if not an identifier, find the token value
    if (tableValue == 1001)
        tempToken.tokenValue = EOFTk;
    if (tableValue == 1002)
        tempToken.tokenValue = IntTk;
    if (tableValue == 1004)
        tempToken.tokenValue = AssignTk;
    if (tableValue == 1005)
        tempToken.tokenValue = ComparTk;
    if (tableValue == 1006)
        tempToken.tokenValue = IncrTk;
    if (tableValue == 1007)
        tempToken.tokenValue = DecrTk;
    if (tableValue == 1008)
        tempToken.tokenValue = ColTk;
    if (tableValue == 1009)
        tempToken.tokenValue = SemColTk;
    if (tableValue == 1010)
        tempToken.tokenValue = PeriodTk;
    if (tableValue == 1011)
        tempToken.tokenValue = ComTk;
    if (tableValue == 1012)
        tempToken.tokenValue = GTEQTk;
    if (tableValue == 1013)
        tempToken.tokenValue = LSEQTk;
    if (tableValue == 1014)
        tempToken.tokenValue = FSlashTk;
    if (tableValue == 1015)
        tempToken.tokenValue = StarTk;
    if (tableValue == 1016)
        tempToken.tokenValue = PercentTk;
    if (tableValue == 1017)
        tempToken.tokenValue = LCBraceTk;
    if (tableValue == 1018)
        tempToken.tokenValue = RCBraceTk;
    if (tableValue == 1019)
        tempToken.tokenValue = LSBraceTk;
    if (tableValue == 1020)
        tempToken.tokenValue = RSBraceTk;
    if (tableValue == 1021)
        tempToken.tokenValue = LParenTk;
    if (tableValue == 1022)
        tempToken.tokenValue = RParenTk;
    return tempToken;
}